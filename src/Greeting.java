public class Greeting extends Thread{
    public static void main(String[] args) throws InterruptedException {
        MyThread t = new MyThread();
        CreateThread1 t1 = new CreateThread1(t);
        CreateThread2 t2 = new CreateThread2(t);
        CreateThread3 t3 = new CreateThread3(t);
        CreateThread4 t4 = new CreateThread4(t);
        CreateThread5 t5 = new CreateThread5(t);
//        t1.setPriority(10);
//        t2.setPriority(8);
//        t3.setPriority(6);
//        t4.setPriority(4);
//        t5.setPriority(2);
        t1.start();
        t1.join();
        t2.start();
        t2.join();
        t3.start();
        t3.join();
        t4.start();
        t4.join();
        t5.start();
        t5.join();
    }
}
class MyThread{
    /** print thread1 */
    synchronized void printThread1(String s){
        int n = s.length();
        for(int i=0;i<n;i++){
            System.out.print(s.charAt(i));
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println();
    }
    /** print thread2 */
    synchronized void printThread2(int n){
        for(int i=0;i<n;i++){
            System.out.print("*");
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println();
    }
    /** print thread3 */
    synchronized void printThread3(String s){
        int n = s.length();
        for(int i=0;i<n;i++){
            System.out.print(s.charAt(i));
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println();
    }
    /** print thread4 */
    synchronized void printThread4(int n){
        for(int i=0;i<n;i++){
            System.out.print("-");
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println();
    }
    /** print thread5 */
    synchronized void printThread5(int n){
        System.out.print("Downloading");
        for(int i=0;i<n;i++){
            System.out.print(".");
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.print("Completed 100%!");
        System.out.println();
    }
}
/** class thread1 */
class CreateThread1 extends Thread{
    MyThread thread1;
    CreateThread1(MyThread thread1){
        this.thread1 = thread1;
    }
    public void run(){
        String s = "Hello KHSRD!";
        thread1.printThread1(s);
    }
}
/** class thread2 */
class CreateThread2 extends Thread{
    MyThread thread2;
    CreateThread2(MyThread thread2){
        this.thread2 = thread2;
    }
    public void run(){
        int n = 40;
        thread2.printThread2(n);
    }
}
/** class thread3 */
class CreateThread3 extends Thread{
    MyThread thread3;
    CreateThread3(MyThread thread3){
        this.thread3 = thread3;
    }
    public void run(){
        String s = "I will try my best to be here at HRD.";
        thread3.printThread3(s);
    }
}
/** class thread4 */
class CreateThread4 extends Thread{
    MyThread thread4;
    CreateThread4(MyThread thread4){
        this.thread4 = thread4;
    }
    public void run(){
        int n = 40;
        thread4.printThread4(n);
    }
}
/** class thread5 */
class CreateThread5 extends Thread{
    MyThread thread5;
    CreateThread5(MyThread thread5){
        this.thread5 = thread5;
    }
    public void run(){
        int n = 10;
        thread5.printThread5(n);
    }
}
